<?php
/**
 */

class cf7_api_admin {
    const NONCE = 'cf7_api_admin';


    protected static $initiated = false;

    public static function init() {
        if (!self::$initiated) {
            self::$initiated = true;
            add_action( 'admin_enqueue_scripts', array('cf7_api_admin', 'admin_enqueue_scripts') );
            add_action( 'wpcf7_save_contact_form', array('cf7_api_admin', 'save_contact_form'));
            add_filter( 'wpcf7_editor_panels', array('cf7_api_admin', 'panels'));
        }
    }

    public static function view( $name, array $args = array() ) {
        $args = apply_filters( 'cf7_api_view_arguments', $args, $name );

        foreach ( $args AS $key => $val ) {
            $$key = $val;
        }

        load_plugin_textdomain( 'cf7-api-integration' );

        $file = CF7_API__PLUGIN_DIR . 'views/'. $name . '.php';

        include( $file );
    }

    /**
     * Add a CF7 API setting panel to the contact form admin section.
     *
     * @param array $panels
     * @return array
     */
    public static function panels($panels) {
        $panels['cf7-api-integration'] = array(
          'title' => __( 'CF7 API', 'cf7-api-integration' ),
          'callback' => array('cf7_api_admin', 'api_panel'),
        ) ;
        return $panels;
    }

    public static function api_panel($post) {
        $apicrm = $post->prop('apicrm' );
        cf7_api_admin::view('api_panel', array('post' => $post, 'apicrm' => $apicrm));
    }

    public static function save_contact_form($contact_form) {
        $properties = $contact_form->get_properties();
        $apicrm = $properties['apicrm'];

        $apicrm['enable'] = false;

        if ( isset( $_POST['enable-apicrm'] ) ) {
            $apicrm['enable'] = true;
        }

        $apicrm['skipmail'] = false;
        if ( isset( $_POST['apicrm-skipmail'] ) ) {
            $apicrm['skipmail'] = true;
        }

        if ( isset( $_POST['apicrm-url'] ) ) {
            $apicrm['url'] = trim( $_POST['apicrm-url'] );
        }

        if ( isset( $_POST['apicrm-organization'] ) ) {
            $apicrm['organization'] = trim( $_POST['apicrm-organization'] );
        }

        if ( isset( $_POST['apicrm-key'] ) ) {
            $apicrm['key'] = trim( $_POST['apicrm-key'] );
        }


        if ( isset( $_POST['apicrm-action'] ) ) {
            $apicrm['action'] = trim( $_POST['apicrm-action'] );
        }
        if ( isset( $_POST['apicrm-parameters'] ) ) {
            $apicrm['parameters'] = trim( $_POST['apicrm-parameters'] );
        }

        $properties['apicrm'] = $apicrm;
        $contact_form->set_properties($properties);
    }

    public static function admin_enqueue_scripts($hook_suffix) {
        if ( false === strpos( $hook_suffix, 'wpcf7' ) ) {
            return;
        }
        wp_enqueue_script( 'cf7_api-admin',
          CF7_API__PLUGIN_URL. 'js/admin.js',
          array( 'jquery', 'jquery-ui-tabs' )
        );
    }
}
