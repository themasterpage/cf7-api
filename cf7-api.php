<?php
/**
 * @author Lenny Mann <info@themasterpage.net>
 * @license http://www.gnu.org/licenses/gpl-2.0.html
 */

/*
Plugin Name: Contact Form 7 API CRM Integration
Display Name: Contact Form 7 API CRM Integration
Plugin URI: http://themasterpage.net/apicrm
Description: Submit contact form 7 to an external api
Version: 1.0.0
Author: Lenny Mann
Author URI: http://www.themasterpage.net
License: GPLv2
Text Domain: cf7-apicrm-integration
 */

define( 'CF7_API__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'CF7_API__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

if ( is_admin() ) {
    require_once( CF7_API__PLUGIN_DIR . 'cf7-api-admin.php' );
    add_action( 'init', array( 'cf7_api_admin', 'init' ) );
}

add_filter( 'wpcf7_contact_form_properties', 'contact_form_api_properties');
add_action('wpcf7_before_send_mail', 'cf7_api_before_send_mail');

require_once('modules/kselect.php');
require_once('modules/kcheckbox.php');

function cf7_api_before_send_mail($contact_form) {
    $properties = $contact_form->get_properties();

    if (empty($properties['apicrm']['enable'])) {
        return;
    }

    $errormessage='';
    $submission = WPCF7_Submission::get_instance();


    if (empty($properties['apicrm']['url'])) {
        $errormessage = $errormessage . 'URL CRM API missing ';
    }
    if (empty($properties['apicrm']['key'])) {
        $errormessage = $errormessage . 'Key CRM API missing';
    }
    if (empty($properties['apicrm']['organization'])) {
        $errormessage = $errormessage . 'Organization ID missing';
    }
    if (empty($properties['apicrm']['action'])) {
        $errormessage = $errormessage . 'Action CRM API missing ';
    }

    if(empty($errormessage)){
        $url = $properties['apicrm']['url'];
        $token = $properties['apicrm']['key'];
        $submittedData = $submission->get_posted_data();

        $data = array();
        foreach($submittedData as $key => $val) {
            if (is_array($val)) {
                $val = implode(", ", $val);
            }
            $data[$key] = $val;
        }
        foreach( wpcf7_scan_form_tags( array( 'type' => 'acceptance' ) ) as $shortcode) {
            $data[ $shortcode['name']] = $data[ $shortcode['name']]!='' ? 'true' : 'false';
        }
        foreach( wpcf7_scan_form_tags( array( 'type' => 'checkbox' ) ) as $shortcode) {
            $data[ $shortcode['name']] = $data[ $shortcode['name']]!='' ? 'true' : 'false';
        }
        foreach( wpcf7_scan_form_tags( array( 'type' => 'checkbox*' ) ) as $shortcode) {
            $data[ $shortcode['name']] = $data[ $shortcode['name']]!='' ? 'true' : 'false';
        }

        // $parameters = explode("&", $properties['apicrm']['parameters']);
        // foreach($parameters as $param) {
        //     list($key, $val) = explode("=", $param);
        //     if (!empty($key)) {
        //         $data[$key] = $val;
        //     }
        // }

        // Set the organization ID for the CRM to properly store the lead
        $data['organization_id'] = $properties['apicrm']['organization'];

        //Now add site url to data for CRM form tracking
        $data['url_referer'] = get_site_url();

        //Add http referer
        $data['http_referer'] = wp_get_referer() ? wp_get_referer() : 'direct';

        $response = wp_remote_post( $url, [
            'headers' => [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ],
            'body' => json_encode($data),
        ]);

        if($response['response']['code'] != 200) {
            $errormessage = $errormessage . $response['response']['message'];
        }

        // write_log('data: '.print_r($data, true));
        
        write_log('response: '.json_encode($response));
    } else {
        $echo = json_encode($errormessage);
        if ( wpcf7_is_xhr() ) {
            @header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
            echo $echo;
        } else {
            @header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
            echo '<textarea>' . $echo . '</textarea>';
        }
        exit();
    }

    //$contact_form->skip_mail= !empty($properties['apicrm']['skipmail']);
    function my_skip_email($f) {
        if(!empty($properties['apicrm']['skipmail']) ) {
            return true; //Do not send email
        }
     } 

    add_filter('wpcf7_skip_mail','my_skip_email');


}

function contact_form_api_properties($properties) {

    if (!isset($properties['apicrm'])) {
        $properties['apicrm'] = array(
          'skipmail' => true,
          'enable' => false,
          'url' => 'http://',
          'organization' => '',
          'key' => '',
          'action' => 'post',
          'parameters' => ''
        );
    }
    return $properties;
}

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_ksubmit' ,9000);

function wpcf7_add_shortcode_ksubmit() {
	wpcf7_add_form_tag( 'submit', 'wpcf7_ksubmit_shortcode_handler' );
}

function wpcf7_ksubmit_shortcode_handler( $tag ) {
    $validation_error = wpcf7_get_validation_error( 'submit' );
    $html = sprintf('<span class="wpcf7-form-control-wrap %1$s">' . wpcf7_submit_shortcode_handler( $tag ) . '%2$s</span>',sanitize_html_class('submit' ),$validation_error );
    return $html;
}
