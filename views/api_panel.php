<?php
/**
 * @author Lenny Mann <info@themasterpage.net>
 * @license http://www.gnu.org/licenses/gpl-2.0.html
 */
?>
<h3><?php echo esc_html( 'Enable API Post'); ?></h3>

<div class="contact-form-editor-box-api">

  <p><label for="enable-apicrm"><input type="checkbox" id="enable-apicrm" name="enable-apicrm" class="toggle-form-table" value="1"<?php echo ( ! empty( $apicrm['enable'] ) ) ? ' checked="checked"' : ''; ?> /> <?php echo esc_html( 'Enable API post integration' ); ?></label></p>

<fieldset>

  <legend><?php echo esc_html("Use this form to set the CF7 API call");?></legend>
  <label><?php echo esc_html(""); ?></label>

  <table class="form-table">
    <tbody>

    <tr>
      <th scope="row">
        <label for="url"><?php echo esc_html( 'API URL'); ?></label>
      </th>
      <td>
        <input type="text" id="apicrm-url" name="apicrm-url" class="large-text code" size="70" value="<?php echo esc_attr( $apicrm['url'] ); ?>" />
      </td>
    </tr>
    <tr>
      <th scope="row">
        <label for="organization"><?php echo esc_html('Organization ID'); ?></label>
      </th>
      <td>
        <input type="text" id="apicrm-organization" name="apicrm-organization" class="large-text code" size="70" value="<?php echo esc_attr( $apicrm['organization'] ); ?>" />
      </td>
    </tr>
    <tr>
      <th scope="row">
        <label for="key"><?php echo esc_html( 'Authorization Token'); ?></label>
      </th>
      <td>
        <input type="text" id="apicrm-key" name="apicrm-key" class="large-text code" size="120" value="<?php echo esc_attr( $apicrm['key'] ); ?>" /><br />
        <label>Key value"</label>
      </td>
    </tr>
    <tr>
      <th scope="row">
        <label for="action"><?php echo esc_html( 'Action'); ?></label>
      </th>
      <td>
        <input type="text" id="apicrm-action" name="apicrm-action" class="large-text code" size="70" value="<?php echo esc_attr( $apicrm['action'] ); ?>" />
      </td>
    </tr>
    <tr>
      <th scope="row">
        <label for="parameters"><?php echo esc_html('Parameters'); ?></label>
      </th>
      <td>
        <input type="text" id="apicrm-parameters" name="apicrm-parameters" class="large-text code" size="70" value="<?php echo esc_attr( $apicrm['parameters'] ); ?>" />
      </td>
    </tr>
    <tr>
      <th scope="row">
        <label for="skipmail"><?php echo esc_html( 'Disable sending mail via Contact Form 7'); ?></label>
      </th>
      <td>
        <input type="checkbox" id="apicrm-skipmail" name="apicrm-skipmail" value="1"<?php echo ( ! empty( $apicrm['skipmail'] ) ) ? ' checked="checked"' : ''; ?> />
      </td>
    </tr>
    </tbody>
  </table>

</fieldset>

</div>
