=== Contact Form 7 - CF7 API addon
Contributors: lennymann
Tags: contact form 7, cf7, api, crm 
Requires at least: 4.7
Tested up to: 4.7
Stable tag: 1.0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Contact Form 7 add-on that will capture leads and send form data to a specified API application.

== Description ==

This plugin will enable the admin to send contact form lead data to an external application via API call for capture.
The plugin will allow the form to be sent to both email and API or just API.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Enable Contact Form 7 API CRM Integration on per form basis.

== Usage ==
1. Create a form or edit an existing one using Contact Form 7 interface.
2. On the far righ tabs menu, click on the "CF7 API" tab.
3. Fill out the appropriate fields, especially API URL and Authorization Token. (you will need a token provided by your API application)

== Changelog ==

= 1.0.0 =
* Initial commit