<?php
/**
 ** A base module for [kcheckbox], [kcheckbox*], and [kradio]
 **/

/* Shortcode handler */

add_action( 'wpcf7_init', 'wpcf7_add_shortcode_kcheckbox' );

function wpcf7_add_shortcode_kcheckbox() {
	wpcf7_add_form_tag( array( 'kcheckbox', 'kcheckbox*', 'kradio' , 'kradio*' ),
		'wpcf7_kcheckbox_shortcode_handler', true );
}


function wpcf7_kcheckbox_shortcode_handler( $tag ) {
	$tag = new WPCF7_FormTag( $tag );

	if ( empty( $tag->name ) )
		return '';

	$validation_error = wpcf7_get_validation_error( $tag->name );

    $tag->basetype=  'kradio' == $tag->basetype ? 'radio':'checkbox';

	$class = wpcf7_form_controls_class( $tag->basetype );

	if ( $validation_error )
		$class .= ' wpcf7-not-valid';

	$label_first = $tag->has_option( 'label_first' );
	$use_label_element = $tag->has_option( 'use_label_element' );
	$exclusive = $tag->has_option( 'exclusive' );
	$free_text = $tag->has_option( 'free_text' );
	$multiple = false;


	if ( 'checkbox' == $tag->basetype )
		$multiple = ! $exclusive;
	else // radio
		$exclusive = false;

	if ( $exclusive )
		$class .= ' wpcf7-exclusive-checkbox';

	$atts = array();

	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();

	$tabindex = $tag->get_option( 'tabindex', 'int', true );

	if ( false !== $tabindex )
		$tabindex = absint( $tabindex );

	$html = '';
	$count = 0;

	$values = (array) $tag->values;
	$labels = (array) $tag->labels;




    /************************************************/

    $errormessage='';
    $contact_form = WPCF7_ContactForm::get_current();
    $properties = $contact_form->get_properties();
    $controller=$tag->get_option( 'resource', '', true );
    $action='lookup';
    $url = $properties['apicrm']['url'].'/eapi/'.$properties['apicrm']['key'].'/'.$controller.'/'.$action.'/';

    if (empty($properties['apicrm']['enable'])) {
        return;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //$HTTP_REFERER =$_SERVER['HTTP_REFERER'];
    $HTTP_REFERER =($_SERVER['HTTPS']!='off' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/';
    curl_setopt($ch, CURLOPT_REFERER, $HTTP_REFERER);
    curl_setopt($ch, CURLOPT_URL, $url);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length:' . strlen($datajson)) );
    //curl_setopt($ch, CURLOPT_POSTFIELDS, $datajson);

    if (!($json = curl_exec($ch))){

        $errormessage= $errormessage . curl_error($ch);

    }
    else
    {
        $http_code= curl_getinfo($ch)['http_code'];
        if ($http_code!=200)
        {
            $errormessage= $errormessage . $properties['messages']['mail_sent_ng'] . strval("(return code: {$http_code})");
        }

    }
    $data=array();
    curl_close($ch);
    if(empty($errormessage)){
        $return=json_decode($json,true);
        if (!empty($return[0]['error'])){
            $errormessage= $errormessage . $return[0]['errorMessage'];
        }
        else  $data=$return[0]['data'];
    }

    if(empty($errormessage)){
        foreach( $data as $rec) {
            array_push($values,$rec['id']);
            array_push($labels,$rec['text']);
        }
    }
    else
    {
        ////TO DO //////////////
    }
    /************************************************/



	if ( $data = (array) $tag->get_data_option() ) {
		if ( $free_text ) {
			$values = array_merge(
				array_slice( $values, 0, -1 ),
				array_values( $data ),
				array_slice( $values, -1 ) );
			$labels = array_merge(
				array_slice( $labels, 0, -1 ),
				array_values( $data ),
				array_slice( $labels, -1 ) );
		} else {
			$values = array_merge( $values, array_values( $data ) );
			$labels = array_merge( $labels, array_values( $data ) );
		}
	}

	$defaults = array();

	$default_choice = $tag->get_default_option( null, 'multiple=1' );

	foreach ( $default_choice as $value ) {
		$key = array_search( $value, $values, true );

		if ( false !== $key ) {
			$defaults[] = (int) $key + 1;
		}
	}

	if ( $matches = $tag->get_first_match_option( '/^default:([0-9_]+)$/' ) ) {
		$defaults = array_merge( $defaults, explode( '_', $matches[1] ) );
	}

	$defaults = array_unique( $defaults );

	$hangover = wpcf7_get_hangover( $tag->name, $multiple ? array() : '' );

	foreach ( $values as $key => $value ) {
		$class = 'wpcf7-list-item';

		$checked = false;

		if ( $hangover ) {
			if ( $multiple ) {
				$checked = in_array( esc_sql( $value ), (array) $hangover );
			} else {
				$checked = ( $hangover == esc_sql( $value ) );
			}
		} else {
			$checked = in_array( $key + 1, (array) $defaults );
		}

		if ( isset( $labels[$key] ) )
			$label = $labels[$key];
		else
			$label = $value;

		$item_atts = array(
			'type' => $tag->basetype,
			'name' => $tag->name . ( $multiple ? '[]' : '' ),
			'value' => $value,
			'checked' => $checked ? 'checked' : '',
			'tabindex' => $tabindex ? $tabindex : '' );

		$item_atts = wpcf7_format_atts( $item_atts );

		if ( $label_first ) { // put label first, input last
			$item = sprintf(
				'<span class="wpcf7-list-item-label">%1$s</span>&nbsp;<input %2$s />',
				esc_html( $label ), $item_atts );
		} else {
			$item = sprintf(
				'<input %2$s />&nbsp;<span class="wpcf7-list-item-label">%1$s</span>',
				esc_html( $label ), $item_atts );
		}

		if ( $use_label_element )
			$item = '<label>' . $item . '</label>';

		if ( false !== $tabindex )
			$tabindex += 1;

		$count += 1;

		if ( 1 == $count ) {
			$class .= ' first';
		}

		if ( count( $values ) == $count ) { // last round
			$class .= ' last';

			if ( $free_text ) {
				$free_text_name = sprintf(
					'_wpcf7_%1$s_free_text_%2$s', $tag->basetype, $tag->name );

				$free_text_atts = array(
					'name' => $free_text_name,
					'class' => 'wpcf7-free-text',
					'tabindex' => $tabindex ? $tabindex : '' );

				if ( wpcf7_is_posted() && isset( $_POST[$free_text_name] ) ) {
					$free_text_atts['value'] = wp_unslash(
						$_POST[$free_text_name] );
				}

				$free_text_atts = wpcf7_format_atts( $free_text_atts );

				$item .= sprintf( ' <input type="text" %s />', $free_text_atts );

				$class .= ' has-free-text';
			}
		}

		$item = '<span class="' . esc_attr( $class ) . '">' . $item . '</span>';
		$html .= $item;
	}

	$atts = wpcf7_format_atts( $atts );

	$html = sprintf(
		'<span class="wpcf7-form-control-wrap %1$s"><span %2$s>%3$s</span>%4$s</span>',
		sanitize_html_class( $tag->name ), $atts, $html, $validation_error );

	return $html;
}

/* Validation filter */
add_filter( 'wpcf7_validate_kcheckbox', 'wpcf7_checkbox_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_kcheckbox*', 'wpcf7_checkbox_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_kradio', 'wpcf7_checkbox_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_kradio*', 'wpcf7_checkbox_validation_filter', 10, 2 );

/* Adding free text field */
add_filter( 'wpcf7_posted_data', 'wpcf7_checkbox_posted_data' );

/* Tag generator */

if ( is_admin() ) {
	add_action( 'admin_init', 'wpcf7_add_tag_generator_kcheckbox_and_kradio', 30 );
}

function wpcf7_add_tag_generator_kcheckbox_and_kradio() {

	if( class_exists('WPCF7_TagGenerator') ) {

		$tag_generator = WPCF7_TagGenerator::get_instance();
        $tag_generator->add( 'kcheckbox', __( 'Checkboxes (API CRM)', 'contact-form-7' ),
            'wpcf7_tag_generator_kcheckbox' );
        $tag_generator->add( 'kradio', __( 'Radio buttons (API CRM)', 'contact-form-7' ),
            'wpcf7_tag_generator_kcheckbox' );
	}
}

function wpcf7_tag_generator_kcheckbox( $contact_form, $args = '' ) {
	$args = wp_parse_args( $args, array() );
	$type = $args['id'];

	if ( 'kradio' != $type ) {
		$type = 'kcheckbox';
	}
    $description='';
	if ( 'kcheckbox' == $type ) {
		$description =  "Generates a form-tag for a group of check boxes retrieving the values ​​from connected CRM.";
	} elseif ( 'kradio' == $type ) {
		$description =  "Generates a form-tag for a group of radio buttons by retrieving values ​​from connected CRM.";
	}

?>
<div class="control-box">
    <fieldset>
        <legend><?php echo  esc_html( $description ); ?></legend>

        <table class="form-table">
            <tbody>

                <tr>
                    <th scope="row"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></legend>
                            <label>
                                <input type="checkbox" name="required" />
                                <?php echo esc_html( __( 'Required field', 'contact-form-7' ) ); ?></label>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="<?php echo esc_attr( $args['content'] . '-name' ); ?>"><?php echo esc_html( __( 'Name', 'contact-form-7' ) ); ?></label></th>
                    <td>
                        <input type="text" name="name" class="tg-name oneline" id="<?php echo esc_attr( $args['content'] . '-name' ); ?>" /></td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="<?php echo esc_attr( $args['content'] . '-resource' ); ?>"><?php echo esc_html( __( 'Resource', 'contact-form-7' ) ); ?></label></th>
                    <td>
                        <input type="text" name="resource" class="option" id="<?php echo esc_attr( $args['content'] . '-resource' ); ?>" /></td>
                </tr>
                <tr>
                    <th scope="row"><?php echo esc_html( __( 'Options', 'contact-form-7' ) ); ?></th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><?php echo esc_html( __( 'Options', 'contact-form-7' ) ); ?></legend>
                            <label>
                                <input type="checkbox" name="label_first" class="option" />
                                <?php echo esc_html( __( 'Put a label first, a checkbox last', 'contact-form-7' ) ); ?></label><br />
                            <label>
                                <input type="checkbox" name="use_label_element" class="option" />
                                <?php echo esc_html( __( 'Wrap each item with label element', 'contact-form-7' ) ); ?></label>
                            <?php if ( 'kcheckbox' == $type ) : ?>
                            <br />
                            <label>
                                <input type="checkbox" name="exclusive" class="option" />
                                <?php echo esc_html( __( 'Make checkboxes exclusive', 'contact-form-7' ) ); ?></label>
                            <?php endif; ?>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="<?php echo esc_attr( $args['content'] . '-id' ); ?>"><?php echo esc_html( __( 'Id attribute', 'contact-form-7' ) ); ?></label></th>
                    <td>
                        <input type="text" name="id" class="idvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-id' ); ?>" /></td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="<?php echo esc_attr( $args['content'] . '-class' ); ?>"><?php echo esc_html( __( 'Class attribute', 'contact-form-7' ) ); ?></label></th>
                    <td>
                        <input type="text" name="class" class="classvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-class' ); ?>" /></td>
                </tr>

            </tbody>
        </table>
    </fieldset>
</div>

<div class="insert-box">
    <input type="text" name="<?php echo $type; ?>" class="tag code" readonly="readonly" onfocus="this.select()" />

    <div class="submitbox">
        <input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
    </div>
</div>
<?php
}

?>
